#!/usr/bin/env python
##
#  play-with-domtables.py 
#
## Overview 
# 
# Go to url and simulate nav with Browser and Cookiejar
# When open url, transform normalized DOMElement into readable XML with BeautifulSoup
# Get table information and put into dictionary, filter by state
# This dictionary is use to iterate again between all elements and use once again libs Browser, Cookiejar
# This second table get problems with form DOMElement and this script takes this DOM and build again html and parse with BeautifulSoup
# Combine both dictionaries and make an multi-dimension dictionary with all the results and values
# 

from bs4 import BeautifulSoup
from mechanize import Browser
from datetime import datetime, date, timedelta
from hashlib import md5
from collections import defaultdict
from threading import Thread

try:
    from http.cookiejar import CookieJar
except ImportError:
    from cookielib import CookieJar

import hashlib, os
import sys
import time
import logging
import traceback
import argparse
import datetime

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
route = "/tmp/"
#handler = logging.FileHandler(route + "<somelog>")
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s|%(thread)s|%(levelname)s|%(threadName)s|%(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)
logger.info("Start script %s" % (__name__))

user= "<some_user>" 
password = "<some_password>"
url = "<some_url>" 

############### class
class Threading(Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, *, daemon=None):
        Thread.__init__(self, group, target, name, args, kwargs, daemon=daemon)

        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)

    def join(self):
        Thread.join(self)
        return self._return

############## funciotns
def toMD5(clave):
    passw = clave.encode('utf-8')
    return md5(passw).hexdigest()

def mergeData(name):
    both = {}
    logger.info("Thread %s: starting", name)
    
    urlvplan = "ot/vplanned.php?id="

    # Get elements for something table into readable data
    logger.info("redirect url to -> %s", url + urlvplan + tp)
    request3 = browser.open(url + urlvplan + tp)
    logger.debug("build url and get data from : %s \n %s", url + urlvplan + tp, request3.info())
    dom = request3.read()

    logger.info("get HTML DOMElement")
    soup = BeautifulSoup(dom, "lxml")
    logger.debug("body html is => \n %s", soup.prettify())

    data = []
    element = []
    divtoseek = "vistaEstandar"
    elem = soup.findAll("div", {"id": divtoseek , "name": divtoseek })

    # Fix bull shit, replaces tr td table into malformed HTML
    domrw = str(elem).replace("</tr>","</tr><tr>").replace("<th>","<td>").replace("</th>","</td>").replace("<td></td></table>","<td></td></tr></table>") 

    logger.info("rebuild string to domelements for nav into table elements")
    soup2 = BeautifulSoup(domrw, "lxml")
    rows = soup2.findAll('tr')

    domlookup = "Elementos"
    logger.info("get unique table in HTML and transform td into dictionary data")
    for row in rows:
        cols = row.findAll('td')
        cols = [ele.text.strip() for ele in cols]
        data.append([ele for ele in cols if ele]) # Get rid of empty values

    # Transform into dictionary readable
    elem = {}

    count = 0
    logger.info("iterate td values and obtain values if data is in domlookup and get %s" % (domlookup) )
    for ix in range(len(data)):
        if ix == 0:
            #range 0 is name of elements
            continue
        for xi in range(len(data[ix])):
            #make dynamic index (data always not be the same, thanks allware)
            idx = str(data[0][xi]).replace(" ","_").lower() 
            elem[idx] = data[ix][xi]
        #counting
    count = count + 1 

    # Merging both elements 
    pdict = pdicts[0]
    both = dict(list(pdict.items()) + list(elem.items()))

    time.sleep(1)
    logger.info("Thread %s: finishing", name)
    return both

############## main
is_planned = False
password = toMD5(password)

browser = Browser()
cookiefile = CookieJar()
browser.set_cookiejar(cookiefile)

logger.info("url to navigate is -> %sindex.php", url)
browser.open(url + "index.php")
browser.response()
logger.info("try to login into -> %sindex.php", url)

browser.select_form(nr=0)
browser.form.set_all_readonly(False)

# md5, usuario and password are the parameters who url is required
browser.form["md5"] = str(password)
browser.form["usuario"] = user
browser.form["password"] = str(toMD5(password))

urlplanassign = "ot/plannedassigned.php?item=6&order=desc"
browser.submit()
logger.info("redirect url to -> %s", url + urlplanassign)
request2 = browser.open(url + urlplanassign)
logger.debug("build url and get data from : %s \n %s", url + urlplanassign, request2.info())
dom = request2.read()

logger.info("get HTML DOMElement")
soup = BeautifulSoup(dom, "lxml")
logger.debug("body html is => \n %s", soup.prettify())

data = []
planned = []

table = soup.find('table')
rows = table.findAll('tr')

logger.info("get unique table in urlplanassign and transform td into dictionary data")
for row in rows:
    cols = row.findAll('td')
    #print(cols)
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols if ele]) # Get rid of empty values

state = "ROLLBACK"
logger.info("iterate td values and obtain values if data is in %s" % (state) )
for i in range(len(data)):
    for x in range(len(data[i])):
        if str(data[i][x]) == state:
            is_planned = True
            planned.append(data[i])

# turn planned data into dictionary and readable value
pdicts = {}

count = 0
for i in range(len(planned)):
    # into dictionary, create a new dictionary for index
    pdicts[count] = {}
    # insert data into index
    pdicts[count]["tp_id"] = planned[i][0]
    pdicts[count]["title"] = planned[i][1]
    pdicts[count]["state"] = planned[i][2]
    pdicts[count]["impact"] = planned[i][3]
    pdicts[count]["date_execution"] = planned[i][4]
    pdicts[count]["date_closed"] = planned[i][5]
    pdicts[count]["date_accept"] = planned[i][6]
    pdicts[count]["job"] = planned[i][7]
    # counting 
    count = count + 1 

detail = {}
logger.info("take %s psg_tp and combine with info obtained in table urlvplan" % (count))

threads = list()
for i in range(len(pdicts)):
    tp = pdicts[i]["tp_id"]

    #h = hashlib.md5( b"<something_to_hex>")
    #print(h.hexdigest())
    # Throw each tp_id and iterate into mergeTpData
    vrplanned = Threading(target=mergeTpData, args=(tp,), name=(tp))
    vrplanned.start()
  
    # Transform into dictionary readable
    detail[i] = vrplanned.join()

    if i > 1:
        break

# close browser 
browser.close()

# new elements try to insert 
for tp in detail.keys():
	insert = """INSERT INTO TP (id,period,title,platform,status,troubles,babysitting,planned_date,execution_date,task,cancel_reason) VALUES (default,"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s");""" % (detail[tp]["tp_id"],detail[tp]["title"],detail[tp]["elementos"],detail[tp]["state"],"no","no",detail[tp]["fecha_creacion"],detail[tp]["date_execution"],detail[tp]["job"],"null")
	#print(detail[tp])
	print(insert)

logger.info("End script %s" % (__name__))
sys.exit(1)
