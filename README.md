# python-scripts

## Overview

`check_kayako_tagisnull.py`
This pyscript takes data from database of kayako reports and compare if open tickets dont have tags uses mysql db conn and transform a few elements in lists to iterate

`play-with-domtables.py`
This script make various actions 

- Go to url and simulate nav with Browser and Cookiejar
- When open url, transform normalized DOMElement into readable XML with BeautifulSoup
- Get table information and put into dictionary, filter by state
- This dictionary is use to iterate again between all elements and use once again libs Browser, Cookiejar
- This second table get problems with form DOMElement and this script takes this DOM and build again html and parse with BeautifulSoup
- Combine both dictionaries and make an multi-dimension dictionary with all the results and values.
- Works with threads when obtain different work orders
- Create inserts for table.


## Instalation

`check_kayako_tagisnull.py`

```
python3
pip3.7 install nagiosplugin \
python-nagios-helpers \
mysqlclient python-mysqldb
```

`play-with-domtables.py`

```
pip3.7 install bs4 \
thread browser hashlib 
```

## Dependencies

```
bs4
nagiosplugin
python-nagios-helpers
```

## Parameters
### Replaces

`check_kayako_tagisnull.py`

Change this values for ur values, good luck.

| Param       		| Description     			| Label 		|
| --------------------- | ------------------------------------- | --------------------- |
| DBHOST      		| autodescriptive			| required		|
| DBUSER      		| autodescriptive			| required		|
| DBPASSWD		| autodescriptive			| required		|
| DBPORT		| autodescriptive			| required		|
| <your_db_kayako>	| DB name for ur kayako			| required		|
| <departments_replace>	| list of departments in db kayako	| required		|
| <somehow_table>	| your table who storage ur acronyms	| required		|

## Usage

```bash
python3.7 check_kayako_tagisnull.py
```

```bash
python3.7 play-with-domtables.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GPL](//www.gnu.org/licenses/quick-guide-gplv3.html)
