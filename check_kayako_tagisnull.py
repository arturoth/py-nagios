#!/usr/bin/env python
# GPL 2019 Arturoth
#
## Overview
# 
# This pyscript takes data from database of kayako reports and compare if open tickets dont have tags 
# Uses mysql db conn and transform a few elements in lists to iterate
#
## Instalation
# 
# > python3 
# pip3.7 install nagiosplugin python-nagios-helpers mysqlclient python-mysqldb
# 
##  Dependencies
#
# yum install -y https://centos7.iuscommunity.org/ius-release.rpm
# yum update
# yum install -y python36u python36u-libs python36u-devel python36u-pip MySQL-python python36-mysql.x86_64 python36-mysql-debug.x86_64
# pip3.6 install cChardet urllib5 pandas prometheus_client html2json bs4 configparser2
## 
# 
## Dependencies 
# 
# nagiosplugin
# python-nagios-helpers

__author__ = "Arturo I"
__version__ = "0.20190705"
__license__ = "GPL"
__extended__ = "https://www.gnu.org/licenses/quick-guide-gplv3.html"

""" Check Kayako open tickets if this assigned tagnames and validate if this exist in somehow table in somehow database """

DBHOST = "localhost"
DBUSER = "root"
DBPASSWD = "root"
DBPORT = 3306

import sys
import MySQLdb

import argparse
import logging
import nagiosplugin

from MySQLdb import Error

_log = logging.getLogger('nagiosplugin')

kdbname = "<your_db_kayako>"
odbname = "<your_db_with_ur_tags>"

############## quering

ksqlwt = """ SELECT st.ticketid, stg.tagname
FROM swtickets  as st
LEFT JOIN swtaglinks as stl on (st.ticketid = stl.linkid)
LEFT JOIN swtags as stg on (stl.tagid = stg.tagid)
WHERE st.ticketstatusid NOT IN (<departments_replace>)
AND st.departmentid NOT IN (0)
AND stg.tagname IS NOT NULL """ 

ksqlwo = """ SELECT st.ticketid 
FROM swtickets  as st
LEFT JOIN swtaglinks as stl on (st.ticketid = stl.linkid)
LEFT JOIN swtags as stg on (stl.tagid = stg.tagid)
WHERE st.ticketstatusid NOT IN (<departments_replace>)
AND st.departmentid NOT IN (0)
AND stg.tagname IS NULL """

#replaces somehow table with ur table
osql = """ SELECT DISTINCT acronym
FROM <somehow_table>
ORDER BY 1 """

############## classes
##### made by arturoth
class db(object):
    __instance   = None
    __host       = None
    __user       = None
    __password   = None
    __database   = None
    __session    = None
    __connection = None
    __port       = None

    def __init__(self, host='localhost', user='root', password='', database='', port='3306'):
        self.__host     = host
        self.__user     = user
        self.__password = password
        self.__database = database
        self.__port     = port
    ## End def __init__

    def __open(self):
        try:
            cnx = MySQLdb.connect(self.__host, self.__user, self.__password, self.__database, self.__port)
            self.__connection = cnx
            self.__session    = cnx.cursor()
        except MySQLdb.Error as e:
            print("error %d: %s" % (e.args[0],e.args[1]))
    ## End def __open
    
    def __close(self):
        self.__session.close()
        self.__connection.close()
    ## End def __close

    def select(self, query):
        self.__open()
        self.__session.execute(query)

        number_rows = self.__session.rowcount
        number_columns = len(self.__session.description)

        if number_rows >= 1 and number_columns > 1:
            result = [item for item in self.__session.fetchall()]
        else:
            result = [item[0] for item in self.__session.fetchall()]
        self.__close()

        return result
    ## End def select

#class Nagios(nagiosplugin.Resource):
class Nagios(object):

    def __init__(self,kdb,odb,ksqlwo,ksqlwt,osql):
        self.__kdb = kdb
        self.__odb = odb
        self.__ksqlwo = ksqlwo
        self.__ksqlwt = ksqlwt
        self.__osql = osql

    def probe(self):
        error = False
        is_wrongtag = False
        err_type = []
        krows = self.__kdb.select(self.__ksqlwo)
        kcount = len(krows)

        if kcount > 0:
            error = True
            err_type.append("notag")
            ticketid = (','.join(map(str, krows)))
            nmsg = ("Existen %s tickets que no tienen asignados tags\n\tTickets afectados, " % (kcount))

        krows = self.__kdb.select(self.__ksqlwt)
        kcount = len(krows)

        if kcount > 0:
            olist = self.__odb.select(self.__osql)

            ticketidtg = []
            for row in krows:
                tid = row[0]
                tag = row[1]

                if tag not in olist:
                    error = True
                    is_wrongtag = True
                    ticketidtg.append(tid)

            if is_wrongtag == True:
                err_type.append("wrongtag")

            tcount = len(ticketidtg)
            ticketidtg = (','.join(map(str, ticketidtg)))

            nmsg2 = ("Hay %s tickets que tienen tag asignado incorrectamente y/o no existe el registro en `prod_overseer`.`Platform`\n\tTickets con tag que no corresponden, " % (tcount))

        if error == True:
            err = 2

            if 'notag' in err_type and 'wrongtag' in err_type:
                data = ("ERR - %s %s \n%s %s") % (nmsg,ticketid,nmsg2,ticketidtg)
            elif 'notag' in err_type:
                data = ("WARN - %s %s") % (nmsg,ticketid)
            else:
                data = ("ERR - %s %s") % (nmsg2,ticketidtg)
        else:
            err = 0
            data = ("OK - Tickets abiertos con tags asignados correctamente")
            
        #metric = nagiosplugin.Metric(data, err, context='default' )
        #return metric 
        print(data)
        sys.exit(err)


############## funciotns
def parse_args():
    argp = argparse.ArgumentParser()
    argp.add_argument('-v', '--verbose', action='count', default=0,
                      help='increase output verbosity (use up to 3 times)')
    argp.add_argument('-t', '--timeout', default=10,
                      help='abort execution after TIMEOUT seconds')
    return argp.parse_args()

############## main
@nagiosplugin.guarded
def main(): 
    args = parse_args()
    
    kdb = db(DBHOST,DBUSER,DBPASSWD,kdbname,DBPORT)
    odb = db(DBHOST,DBUSER,DBPASSWD,odbname,DBPORT)

    response = Nagios(kdb,odb,ksqlwo,ksqlwt,osql)
    response.probe()

    #check = nagiosplugin.Check( Nagios(kdb,odb,ksqlwo,ksqlwt,osql) ) 
    #check.add(nagiosplugin.ScalarContext('default', '', '@1:1'))
    #check.main(args.verbose, args.timeout) 

if __name__ == '__main__':
    main()


